package com.rfl.bookshelf.repos;

import com.rfl.bookshelf.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByTitleStartsWithIgnoreCase(String text);
    Book findBookById(Long id);
}
