package com.rfl.bookshelf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookShelf {

	public static void main(String[] args) {
		SpringApplication.run(BookShelf.class, args);
	}
}
