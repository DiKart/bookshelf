package com.rfl.bookshelf.vaadin.edit;

import com.rfl.bookshelf.entities.Book;
import com.rfl.bookshelf.repos.BookRepository;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@UIScope
@SpringComponent
public class BookController extends VerticalLayout {

    public interface ChangeHandler {void onChange();}

    private final BookRepository bookRepository;
    private Book currentBook;

    Binder<Book> binder = new Binder<>(Book.class);

    TextField title = new TextField("Название");
    TextField description = new TextField("Краткое описание");
    TextField author = new TextField("Автор");
    TextField isbn = new TextField("Номер ISBN");
    TextField printYear = new TextField("Год издания");

    Button save = new Button("Сохранить", FontAwesome.SAVE);
    Button cancel = new Button("Отмена");
    Button delete = new Button("Удалить", FontAwesome.TRASH_O);
    CssLayout actions = new CssLayout(save, cancel, delete);

    @Autowired
    public BookController(BookRepository bookRepository){
        this.bookRepository = bookRepository;

        addComponents(title, description, author, isbn, printYear, actions);
        binder.forField(printYear).withConverter(new StringToIntegerConverter("Введите год"))
                .bind(Book::getPrintYear, Book::setPrintYear);
        binder.bindInstanceFields(this);

        actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        save.addClickListener(e -> bookRepository.save(currentBook));
        cancel.addClickListener(e -> setVisible(false));
        delete.addClickListener(e -> bookRepository.delete(currentBook));
        setVisible(false);

    }

    public final void createBook(Book b){
        if (b == null) {
            setVisible(false);
            return;
        }
        currentBook = b;
        binder.setBean(currentBook);
        setVisible(true);
        author.setEnabled(true);
        save.focus();
        title.selectAll();
    }

    public final void editBook(Book b){
        if (b == null){
            setVisible(false);
            return;
        }
        boolean isInDatabase = b.getId() != null;
        if (isInDatabase) {
            currentBook = bookRepository.findBookById(b.getId());
            author.setEnabled(false);
        }
        else {
            setVisible(false);
            return;
        }
        if (!b.isReadAlready())
            b.setReadAlready(true);
        cancel.setVisible(isInDatabase);
        binder.setBean(currentBook);
        setVisible(true);
        save.focus();
        title.selectAll();
    }
    public void setChangeHandler(ChangeHandler handler){
        save.addClickListener(e -> handler.onChange());
        delete.addClickListener(e -> handler.onChange());
    }
}
