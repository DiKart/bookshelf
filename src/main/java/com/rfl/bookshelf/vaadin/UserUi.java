package com.rfl.bookshelf.vaadin;

import com.rfl.bookshelf.entities.Book;
import com.rfl.bookshelf.repos.BookRepository;
import com.rfl.bookshelf.vaadin.edit.BookController;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

@SpringUI
public class UserUi extends UI{

    final Grid<Book> grid;
    final TextField filter;
    private final Button addButton;

    private final BookRepository repository;
    private final BookController controller;

    @Autowired
    public UserUi(BookRepository repository, BookController controller) {
        this.repository = repository;
        this.controller = controller;

        this.grid = new Grid<>(Book.class);
        this.filter = new TextField();
        this.addButton = new Button("Добавить", FontAwesome.PLUS_CIRCLE);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        HorizontalLayout activities = new HorizontalLayout(filter, addButton);
        VerticalLayout mainLayout = new VerticalLayout(activities, grid, controller);
        setContent(mainLayout);

        grid.setHeight(50, Unit.PERCENTAGE);
        grid.setWidth(1024, Unit.PIXELS);
        grid.setColumns("id", "title", "description", "author", "isbn", "printYear", "readAlready");

        filter.setPlaceholder("Filter by title");
        filter.setValueChangeMode(ValueChangeMode.LAZY);
        filter.addValueChangeListener(e -> listBooks(e.getValue()));

        grid.asSingleSelect().addValueChangeListener(e -> controller.editBook(e.getValue()));

        addButton.addClickListener(e -> controller.createBook(
                new Book("", "", "", "", 0, false)));

        controller.setChangeHandler(() -> {
            controller.setVisible(false);
            listBooks(filter.getValue());
        });
        listBooks(null);

    }
    private void listBooks(String filter){
        if (StringUtils.isEmpty(filter))
            grid.setItems(repository.findAll());
        else
            grid.setItems(repository.findAllByTitleStartsWithIgnoreCase(filter));
    }
}
